import React from "react";
import CreateDrone from "../CreateDrone";
import { shallow } from "enzyme";
import { createDrone } from "../../../api/droneApi";

jest.mock("../../../api/droneApi", () => ({
  createDrone: jest.fn()
}));

describe("Create drone component tests", () => {
  it("Renders without crashing", () => {
    shallow(<CreateDrone />);
  });

  it("Pressing create button should call create drone", () => {
    const wrapper = shallow(<CreateDrone />);
    wrapper.setState({ x: "0", y: "0", quadrant: "0" });
    const button = wrapper.find("button");

    button.simulate("click");
    expect(createDrone).toHaveBeenCalled();
    expect(createDrone).toHaveBeenCalledWith(0, 0, 0);
  });

  it("Calling create with invalid parameters for quadrant should show an error", () => {
    const wrapper = shallow(<CreateDrone />);
    wrapper.setState({ x: "0", y: "0", quadrant: "ABC" });
    const button = wrapper.find("button");

    button.simulate("click");

    expect(wrapper.state().message).toEqual("Quadrant must be a number");
  });
});
