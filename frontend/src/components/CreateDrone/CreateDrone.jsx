import React, { Component } from "react";
import { createDrone } from "../../api/droneApi";

class CreateDrone extends Component {
  constructor(props) {
    super(props);
    this.state = {
      x: "",
      y: "",
      quadrant: "",
      message: ""
    };
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(e) {
    this.setState({
      [e.target.name]: e.target.value
    });
  }

  async createDrone() {
    let quadrant, x, y;
    quadrant = parseInt(this.state.quadrant);
    x = parseFloat(this.state.x);
    y = parseFloat(this.state.y);

    if (isNaN(x)) {
      this.setState({ message: "Coordinate X must be a number" });
    } else if (isNaN(y)) {
      this.setState({ message: "Coordinate Y must be a number" });
    } else if (isNaN(quadrant)) {
      this.setState({ message: "Quadrant must be a number" });
    } else {
      const response = await createDrone(x, y, quadrant);
      this.setState({
        x: "",
        y: "",
        quadrant: "",
        message: response
      });
    }
  }
  render() {
    return (
      <div>
        <label>Enter X coordinate</label>
        <br />
        <input
          name="x"
          placeholder="Enter X coordinate"
          value={this.state.x}
          onChange={this.handleChange}
        />
        <br />
        <label>Enter y coordinate</label>
        <br />
        <input
          name="y"
          placeholder="Enter Y coordinate"
          value={this.state.y}
          onChange={this.handleChange}
        />
        <br />
        <label>Enter quadrant</label>
        <br />
        <input
          name="quadrant"
          placeholder="Enter quadrant"
          value={this.state.quadrant}
          onChange={this.handleChange}
        />
        <br />
        <button onClick={() => this.createDrone()}>Create</button>
        <p>{this.state.message}</p>
      </div>
    );
  }
}

export default CreateDrone;
