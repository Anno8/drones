import React, { Component } from "react";
import "./Quadrant.css";
import {
  droneBroadcastHandler,
  droneLeftTheQuadrantHandler,
  socketDisconnectedHandler,
  socketConnectedHandler
} from "../../modules/socketHandler";
import CreateDrone from "../CreateDrone";

class Quadrant extends Component {
  scaleFactor = 50;
  itemSize = 10;
  canvasSize = this.itemSize * this.scaleFactor;

  constructor(props) {
    super(props);
    this.onDroneBroadcastReceived = this.onDroneBroadcastReceived.bind(this);
    this.onDroneLeftTheQuadrant = this.onDroneLeftTheQuadrant.bind(this);
    this.onSocketDisconnected = this.onSocketDisconnected.bind(this);
    this.onSocketConnected = this.onSocketConnected.bind(this);

    this.canvasRef = React.createRef();
    this.state = {
      droneCount: 0,
      message: ""
    };
  }

  componentDidMount() {
    droneBroadcastHandler(this.onDroneBroadcastReceived);
    droneLeftTheQuadrantHandler(this.onDroneLeftTheQuadrant);
    socketDisconnectedHandler(this.onSocketDisconnected);
    socketConnectedHandler(this.onSocketConnected);
  }

  onDroneLeftTheQuadrant(drone) {
    this.setState({
      message: `Drone with the id of ${drone.id} left the quadrant`
    });
  }

  onSocketConnected() {
    this.setState({ message: "Connected" });
  }

  onDroneBroadcastReceived(data) {
    this.drawCanvas();
    this.setState({ droneCount: data.length });

    data.forEach(({ x, y }) => {
      this.updateCanvas(x, y);
    });
  }

  onSocketDisconnected(e) {
    this.drawCanvas();
    this.setState({ message: "Disconnected..." });
  }

  updateCanvas(x, y) {
    const canvas = this.canvasRef.current;
    const context = canvas.getContext("2d");
    context.rect(
      x * this.scaleFactor,
      y * this.scaleFactor,
      this.scaleFactor,
      this.scaleFactor
    );
    context.fillStyle = "green";
    context.stroke();
    context.fill();
  }

  drawCanvas() {
    const canvas = this.canvasRef.current;
    canvas.height = this.canvasSize;
    canvas.width = this.canvasSize;
    const context = canvas.getContext("2d");
    context.fillStyle = "#FF0000";
    context.fillRect(0, 0, this.canvasSize, this.canvasSize);
  }

  render() {
    return (
      <div className="container">
        <div className="header">
          <h1>Quadrant {this.state.quadrant}</h1>
          <p>Drones in quadrant: {this.state.droneCount}</p>
          <p>{this.state.message}</p>
        </div>
        <CreateDrone />
        <canvas ref={this.canvasRef} />
      </div>
    );
  }
}

export default Quadrant;
