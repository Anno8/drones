import React from "react";
import Quadrant from "../Quadrant";
import { shallow, mount } from "enzyme";

import * as socketHandler from "../../../modules/socketHandler";

jest.mock("../../../modules/socketHandler");

describe("Quadrant tests", () => {
  it("Renders without crashing", () => {
    shallow(<Quadrant />);
  });

  it("Quadrant component", () => {
    const wrapper = shallow(<Quadrant />);
    expect(wrapper).toMatchSnapshot();
  });

  it("When component mounts it should call all the socket handler functions", () => {
    mount(<Quadrant />);

    expect(socketHandler.droneBroadcastHandler).toBeCalled();
    expect(socketHandler.socketDisconnectedHandler).toBeCalled();
    expect(socketHandler.droneBroadcastHandler).toBeCalled();
    expect(socketHandler.droneLeftTheQuadrantHandler).toBeCalled();
  });

  it("When onSocketDisconnected is triggered it should set message", () => {
    // Arrange
    const wrapper = mount(<Quadrant />);
    const instance = wrapper.instance();
    const stateSpy = jest.spyOn(Quadrant.prototype, "setState");
    instance.drawCanvas = jest.fn();

    // Act
    instance.onSocketDisconnected();

    // Assert
    expect(stateSpy).toHaveBeenCalled();
    expect(wrapper.state().message).toEqual("Disconnected...");
  });

  it("When onSocketConnected is triggered it should set message", () => {
    // Arrange
    const wrapper = mount(<Quadrant />);
    const instance = wrapper.instance();
    const stateSpy = jest.spyOn(Quadrant.prototype, "setState");
    instance.drawCanvas = jest.fn();

    // Act
    instance.onSocketConnected();

    // Assert
    expect(stateSpy).toHaveBeenCalled();
    expect(wrapper.state().message).toEqual("Connected");
  });

  it("When onDroneLeftTheQuadrant is triggered it should set message", () => {
    // Arrange
    const wrapper = mount(<Quadrant />);
    const instance = wrapper.instance();
    const stateSpy = jest.spyOn(Quadrant.prototype, "setState");
    instance.drawCanvas = jest.fn();
    const drone = { id: 1, name: "Test" };

    // Act
    instance.onDroneLeftTheQuadrant(drone);

    // Assert
    expect(stateSpy).toHaveBeenCalled();
    expect(wrapper.state().message).toEqual(
      `Drone with the id of ${drone.id} left the quadrant`
    );
  });

  it("When onDroneBroadcastReceived is triggered it should set droneCount", () => {
    // Arrange
    const wrapper = mount(<Quadrant />);
    const instance = wrapper.instance();
    const stateSpy = jest.spyOn(Quadrant.prototype, "setState");
    instance.drawCanvas = jest.fn();
    instance.updateCanvas = jest.fn();
    const data = [{}, {}];

    // Act
    instance.onDroneBroadcastReceived(data);

    // Assert
    expect(stateSpy).toHaveBeenCalled();
    expect(wrapper.state().droneCount).toEqual(data.length);
  });
});
