export const apiUrl =
  process.env.NODE_ENV === "development"
    ? "http://127.0.0.1:8080"
    : "http://127.0.0.1:49876";
