import React from "react";
import Quadrant from "./components/Quadrant";

const App = () => (
  <div>
    <Quadrant />
  </div>
);

export default App;
