import axios from "axios";
import { apiUrl } from "../constants";

export const createDrone = (x, y, quadrant) => {
  return axios
    .post(`${apiUrl}/api/drones`, { x, y, quadrant })
    .then(response => "Drone created.")
    .catch(error => (error.response ? error.response.data : error.message));
};
