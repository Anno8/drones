import io from "socket.io-client";
import { apiUrl } from "../constants";

const options = {
  secure: false,
  rejectUnauthorized: false,
  reconnect: true,
  transports: ["websocket", "polling", "flashsocket"]
};

const socket = io.connect(
  apiUrl,
  options
);
socket.on("error", err => {
  console.log("received socket error:", err);
});

export const socketConnectedHandler = onSocketConnected => 
  socket.on("connect", onSocketConnected);

export const socketDisconnectedHandler = onSocketDisconnected => 
  socket.on("disconnect", onSocketDisconnected);

export const droneBroadcastHandler = onDroneBroadcastReceived =>
  socket.on("droneBroadcast", onDroneBroadcastReceived);

export const droneLeftTheQuadrantHandler = onDroneLeftTheQuadrant =>
  socket.on("droneLeftTheQuadrant", onDroneLeftTheQuadrant);
