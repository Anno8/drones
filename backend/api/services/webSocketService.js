module.exports = (io, socket, droneService) => {
  const clientIntervals = new Map();

  droneService.droneLeftTheQuadrant = drone =>
    socket.emit("droneLeftTheQuadrant", drone);

  const broadcastInterval = setInterval(
    () => socket.emit("droneBroadcast", droneService.get()),
    1000
  );
  clientIntervals.set(socket.id, broadcastInterval);

  socket.on("disconnect", () => {
    const client = clientIntervals.get(socket.id);
    clearInterval(client);
    clientIntervals.delete(socket.id);
  });
};
