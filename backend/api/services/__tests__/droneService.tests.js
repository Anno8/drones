const DroneService = require("../droneService");

const chai = require("chai");
const spies = require("chai-spies");
chai.use(spies);
const expect = chai.expect;
const assert = chai.assert;

describe("Drone service test", () => {
  let droneService = null;
  beforeEach(() => {
    droneService = new DroneService();
  });

  it("Should initialize the drone service", () => {
    expect(droneService.items).to.be.ok;
    expect(droneService.quadrant).to.be.eql({ id: 1, quadrantSize: 9 });
  });

  it("Should create and add a new object when calling create method", () => {
    const newDrone = { x: 1, y: 1, quadrant: 1 };
    const createdDrone = droneService.create(newDrone);
    expect(createdDrone).to.be.eql({ id: 1, x: 1, y: 1, quadrant: 1 });
  });

  it("Should return current items when calling get method", () => {
    const newDrone1 = { x: 1, y: 1, quadrant: 1 };
    const newDrone2 = { x: 1, y: 1, quadrant: 1 };

    droneService.create(newDrone1);
    droneService.create(newDrone2);

    const items = droneService.get();

    expect(items.length).to.be.equal(2);
  });

  it("Should delete the drone when calling delete method", () => {
    const newDrone = { x: 1, y: 1, quadrant: 1 };

    const createdDrone = droneService.create(newDrone);
    droneService.delete(createdDrone.id);
    const items = droneService.get();
    expect(items.length).to.be.equal(0);
  });

  it("Should throw an error if the drone doesn't exist", () => {
    assert(() => droneService.delete(66), "No item exists with an id of: 66");
  });


  it("droneLeftTheQuadrant should be called", () => {
    const drone = { x: 1, y: 1, quadrant: 1 };

    droneService.droneLeftTheQuadrant(drone);
    droneService.droneLeftBroadcast = (drone) => {};
    var spy = chai.spy.on(droneService, 'droneLeftBroadcast');


    expect(spy).to.not.have.been.called(1);
  });
});
