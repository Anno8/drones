const Drone = require("../models/drone");
const utils = require("../utils/utils");

class DroneService {
  constructor() {
    this.items = [];
    this.quadrant = { id: 1, quadrantSize: 9 };
    this.droneLeftBroadcast = null;
  }

  get() {
    return this.items.map(obj => obj.drone);
  }

  create(droneObj) {
    const x = parseFloat(droneObj.x);
    const y = parseFloat(droneObj.y);
    const quadrant = parseFloat(droneObj.quadrant);
    if (isNaN(x) || isNaN(y) || isNaN(quadrant)) {
      throw new Error("Parameters must be a number");
    }
    if (this.quadrant.id !== quadrant) {
      throw new Error(`Quadrant ${quadrant} doesn't exist`);
    }

    const allIds = this.items.map(item => item.drone.getId());
    const id = this.items.length === 0 ? 1 : Math.max(...allIds) + 1;

    const newDrone = new Drone(id, x, y, quadrant);
    this.items.push({
      drone: newDrone,
      interval: setInterval(() => this.updateCoordinates(newDrone), 2000)
    });
    return newDrone;
  }

  delete(id) {
    const itemToDelete = this.items.find(item => item.drone.getId() == id);
    if (itemToDelete) {
      clearInterval(itemToDelete.interval);
      this.items.splice(this.items.indexOf(itemToDelete), 1);
    } else {
      throw new Error(`No item exists with an id of: ${id}`);
    }
  }

  droneLeftTheQuadrant(drone) {
    if (this.droneLeftBroadcast) {
      droneLeftBroadcast(drone);
    }
  }

  updateCoordinates(drone) {
    const direction = utils.getRandomDirection();
    const { verticalMovement, horizontalMovement } = utils.getNewCoordinates(
      direction
    );

    const x = drone.getX() + horizontalMovement;
    const y = drone.getY() + verticalMovement;

    if (horizontalMovement !== 0) {
      const canMoveHorizontally = x <= this.quadrant.quadrantSize && x >= 0;
      if (canMoveHorizontally) {
        drone.update(x, y, this.quadrant.id);
      } else {
        this.droneLeftTheQuadrant(drone);
        this.delete(drone.getId());
      }
    } else {
      const movementResult = y >= 0 && y <= this.quadrant.quadrantSize;
      if (movementResult) {
        drone.update(x, y, this.quadrant);
      } else {
        this.droneLeftTheQuadrant(drone);
        this.delete(drone.getId());
      }
    }
  }
}

module.exports = DroneService;
