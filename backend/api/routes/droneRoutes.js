module.exports = function(app, droneService) {
  app
    .route("/api/drones")
    .get((req, res) => res.send(droneService.get()))
    .post((req, res) => {
      try {
        res.json(droneService.create(req.body));
      } catch (err) {
        res.status(500).send(err.message);
      }
    });

  app
    .route("/api/drones/:id")
    .delete((req, res) => droneService.delete(req.params.id));
};
