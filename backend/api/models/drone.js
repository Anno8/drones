class Drone {
  constructor(id, x, y, quadrant) {
    this.id = id;
    this.x = x;
    this.y = y;
    this.quadrant = quadrant;
  }

  update(x, y, quadrant) {
    this.x = x;
    this.y = y;
    this.quadrant = quadrant;
  }

  getId() {
    return this.id;
  }

  getX() {
    return this.x;
  }

  getY() {
    return this.y;
  }

  getQuadrant() {
    return this.quadrant;
  }
}

module.exports = Drone;
