var expect = require("chai").expect;
const Drone = require("../Drone");

describe("Drone class test", () => {
  it("Should create drone with provided parameters", () => {
    // Arrange
    const id = 1;
    const x = 2;
    const y = 3;

    // Act
    const newDrone = new Drone(id, x, y);

    // Assert
    expect(newDrone.getId()).to.be.equal(id);
    expect(newDrone.getX()).to.be.equal(x);
    expect(newDrone.getY()).to.be.equal(y);
  });

  it("Should update the drone with provided parameters", () => {
    // Arrange
    const x = 2;
    const y = 3;
    const quadrant = 3;

    // Act
    const newDrone = new Drone(5, 5, 5);
    newDrone.update(x, y, quadrant)
    // Assert
    expect(newDrone.getX()).to.be.equal(x);
    expect(newDrone.getY()).to.be.equal(y);
    expect(newDrone.getQuadrant()).to.be.equal(quadrant);
  });
});
