// 1 -> UP; 2 -> RIGHT; 3 -> DOWN; 4 -> LEFT
module.exports.getRandomDirection = () => Math.floor(Math.random() * 4) + 1;
module.exports.getNewCoordinates = direction => {
  let verticalMovement = 0;
  let horizontalMovement = 0;
  switch (direction) {
    case 1:
      verticalMovement = Math.random();
      break;
    case 2:
      horizontalMovement = Math.random();
      break;
    case 3:
      verticalMovement = -Math.random();
      break;
    case 4:
      horizontalMovement = -Math.random();
      break;
  }

  return { verticalMovement, horizontalMovement };
};
