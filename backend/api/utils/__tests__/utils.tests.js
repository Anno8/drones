const utils = require("../utils");

var expect = require("chai").expect;

describe("get random direction tests", () => {
  it("get random direction should return a number between 1 and 4", () => {
    const result = utils.getRandomDirection();

    expect(result).to.be.greaterThan(0);
    expect(result).to.be.lessThan(5);
  });
});

describe("get new coordinates tests", () => {
  it("get new coordinates should return positive verticalMovement for direction 1", () => {
    const { verticalMovement, horizontalMovement } = utils.getNewCoordinates(1);

    expect(verticalMovement).to.satisfy(num => num >= 0 && num <= 1);
    expect(horizontalMovement).to.be.equal(0);
  });

  it("get new coordinates should return positive horizontalMovement for direction 2", () => {
    const { verticalMovement, horizontalMovement } = utils.getNewCoordinates(2);

    expect(horizontalMovement).to.satisfy(num => num >= 0 && num <= 1);
    expect(verticalMovement).to.be.equal(0);
  });

  it("get new coordinates should return negative verticalMovement for direction 3", () => {
    const { verticalMovement, horizontalMovement } = utils.getNewCoordinates(3);

    expect(verticalMovement).to.satisfy(num => num >= -1 && num <= 0);
    expect(horizontalMovement).to.be.equal(0);
  });

  it("get new coordinates should return negative horizontalMovement for direction 4", () => {
    const { verticalMovement, horizontalMovement } = utils.getNewCoordinates(4);

    expect(horizontalMovement).to.satisfy(num => num >= -1 && num <= 0);
    expect(verticalMovement).to.be.equal(0);
  });
});
