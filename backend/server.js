const port = process.env.PORT || 8080;

const express = require("express");
const app = express();

var cors = require("cors");

const server = app.listen(port);
const io = require("socket.io").listen(server);
const bodyParser = require("body-parser");

app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

const DroneService = require("./api/services/droneService");
const droneService = new DroneService();

const WebSocketService = require("./api/services/webSocketService");
io.on("connection", socket => WebSocketService(io, socket, droneService));

const routes = require("./api/routes/droneRoutes");
routes(app, droneService);

console.log(`Server started on port: ${port}`);
